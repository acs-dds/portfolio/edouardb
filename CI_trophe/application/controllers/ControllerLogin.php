<?php

class ControllerLogin extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('ModelLogin');
		$this->load->helper('url');
		$this->load->library('session');
		$this->output->enable_profiler(false);
	}

		// fonction de connection

	public function log() {
		$this->load->library('form_validation');

		$data['titre'] = 'Connexion';
		$data['erreur'] = '';
		
		// rêgles de validation chargé depuis la lbrairie

		$this->form_validation->set_rules('login', 'Login', 'required');
		$this->form_validation->set_rules('mdp', 'Mot de passe', 'required');
		
		$login = $this->input->post('login');
		$mdp = $this->input->post('mdp');

		if (isset($login) && isset($mdp)) {
			
		
			// Si les champs sont vides

			if ($this->form_validation->run() === FALSE) {
				$data['erreur'] = 'veuillez remplir les champs';
			}

			// Si les données match
			
			else if ($this->ModelLogin->check($login, $mdp) == 1) {
				$data['erreur'] = 'Login ou mot de passe incorrect';
				
			}
			else 
			{
				// CODE TEST SESSION
				$this->session->set_userdata('login', $login);

				// MON CODE
				
				redirect('controllerSucces');
			}
		}
		$this->load->view('vue_login', $data);
	}
}

?>