<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerSucces extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
                $this->load->model('ModelSucces');
                $this->load->helper('url');
                $this->output->enable_profiler(false);
                $this->load->library('session');
        }

        // recupère la db dans un tableau et retourne la vue + la db

        public function index() {
            // CODE TEST SESSION
            $idUser = $this->ModelSucces->getUserId($this->session->userdata('login'));
            // CODE TEST SESSION
            $data['trophes'] = $this->ModelSucces->getTrophes($idUser);

            // MON CODE

            // $data['lecture'] = $this->ModelSucces->affiche();

        	$this->load->view('vue_succes', $data);
        }

        // récupère la db et retourne une autre vue

        public function objectif($id) {            
            $data['ligne'] = $this->ModelSucces->obj($id);
            $this->load->view('vue_objectif', $data);
        }
        
        // J'update la progression dans la vue objectif

        public function upd($id) {
            $progression = $this->input->post('quantity');   // =  $_POST['quantity']
            $this->ModelSucces->update($progression, $id);
            redirect(base_url() . "ControllerSucces/objectif/" . $id);
        }

}