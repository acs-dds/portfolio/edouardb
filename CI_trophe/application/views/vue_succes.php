<!DOCTYPE html>
<html>
<head>
	<title>trophé</title>
</head>
<body>

<table>
	<thead>
		<tr>
			<th>Titre</th>
			<th>Progression</th>
			<th>Intitulé</th>
		</tr>
	</thead><article>
<?php foreach ($trophes as $trophe) { ?>
	
	<?php echo '<tr><td>' . $trophe['titre'] . '</td><td>'; ?><progress value="<?php echo $trophe['progression'] ?>" max="<?php echo $trophe['objectif'] ?>"></progress><br><a href="<?php echo site_url("ControllerSucces/objectif/") . $trophe["id"]; ?>">Mettre à jour votre progression</a> <?php echo '</td><td>' . PHP_EOL . $trophe['intitule'] ?></td></tr>
<?php } ?>
	</article>
</table>
</body>
</html>