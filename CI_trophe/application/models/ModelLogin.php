<?php

class ModelLogin extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function check($login, $mdp) {
		$db = $this->db->query("SELECT entreprise,mdp FROM utilisateur WHERE entreprise = ? AND mdp = ?", [$login, hash("sha512", "ok@/" . $mdp)]);

		// vérifie si un mot de passe est présent dans la ligne

		if($db->row("mdp")){
			return 0;
		}
		else{
			return 1;
		}
	}

	
}
?>