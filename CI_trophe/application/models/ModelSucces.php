<?php

class ModelSucces extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// Recupere l'ID de l'utilisateur

	public function getUserId($entreprise) {

		$db = $this->db->query('SELECT id FROM utilisateur WHERE entreprise = ?', [$entreprise]);
		return $db->row('id');
	}

	// Recupere la session en fonction de l'Id

	public function getTrophes($idUser) {

		$db = $this->db->query("SELECT * FROM succes WHERE idutilisateur = ?", [$idUser]);
		return $db->result_array();
		}

	// Affiche le tableau des trophés

	// public function affiche() {
	// $res = $this->db->query("SELECT * FROM succes");
	// return $res->result_array();
	// }

	// recupere une ligne pour vue_objectif

	public function obj($id) {
		$res = $this->db->query("SELECT * FROM succes WHERE id = ?", [$id]);
		return $res->row_array();
	}

	// mise à jour des données dans la vue_objectif

	public function update($progression, $id) {
		$res = $this->db->query("UPDATE succes SET progression = ? WHERE id = ?", [$progression, $id]);
	}


}