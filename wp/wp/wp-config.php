<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'edouardb');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'edouardb');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'W2qgrW73');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i+7(c6)XNPx/+~j^$Z)Wry;v+VQ~M3-/Oq~jd)G}UCbG,GN)TM1jqE9 ,rSI9d~l');
define('SECURE_AUTH_KEY',  '}1^p11TbPpnu>-&YRDCmIQ@d@vs3ms`0^|Wbv6}p#b}io]a9X3G/-< ==P_s}J.=');
define('LOGGED_IN_KEY',    '3,1-XZVG35>dF6=37TX6Y?}nV`1V[xUx%Q&F5n-_/mTA65E2;v-Zu]VbXEB=Le/8');
define('NONCE_KEY',        'dPY A]]Y;GQyo.}w4*XcV!c:bvd{qKkcqLC@:R]Bi @i*]bzixEj^T}=IO/crOM;');
define('AUTH_SALT',        'oi~h,iP;.5[yPtb]lW=J !vvR2PNE.bD@[t0yd:tD&hz%6(t4(XQWUBv}^4>bi&<');
define('SECURE_AUTH_SALT', '[[-`#5~s&$ *twaPA!TG-`:I=1hygC3Ww[.a9-STAzii6owvhzMi]C1@P;1FyOz0');
define('LOGGED_IN_SALT',   'g8{/$+[o]jFnrp(2/G3@obobPU1cG!8>`OJw@[q^<-0lFfK1y:m4 qGJ>bG>yTzm');
define('NONCE_SALT',       '2.MtE0PvkPF>uJ7%!:aq!ww{GJ{26iN|$(oJ^tvR($2M&46R0U}p%fW@*3D1+UvW');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_7xabbcE2A';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');