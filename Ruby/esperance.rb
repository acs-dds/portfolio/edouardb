require 'sinatra'

set :bind, "0.0.0.0"

def page_content(title)
  File.read("pages/#{title}.txt")
rescue Errno::ENOENT
  return nil
end

# Creer un fichier et son contenu directement dans le sous dossier "pages".
# "w" permet d'écrire le contenu au niveau du second argument de la fonction.

def save_content(title, content)
  File.open("pages/#{title}.txt", "w") do |file|
    file.print(content)
  end
end

get "/" do
	erb :titre
end

# le titre en paramètre dans l'URL est associé à une variable ainsi que le contenu du même nom.

get "/:title" do # titre de la page.erb
	@title = params[:title]
	@content = page_content(@title) # titre homonyme.txt
	erb :esperance
end

# {"departement"=>"1", "gender"=>"female", "age"=>"4"}

post "/create" do
	params.inspect # .inspect = le var_dump de ruby
end