require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require "./calc.rb"

enable :logging
set :environment, :development
set :port, 4567