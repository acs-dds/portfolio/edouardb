<?php require 'classes/classeClients.php';
session_start();
require 'classes/classeArticle.php';
require 'classes/mappeur.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type="image/png" href="favicon.png" />
	<title>NODEX | Catalogue</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
</head>
<body class="body_catalogue">
	<main>
		<article>
				<h1><?php echo "Bonjour ".$_SESSION["client"]->getPrenom()." ".$_SESSION["client"]->getNom(); ?></h1>
				<?php $catalogue = Mapper::getCatalogue($_SESSION["client"]->getTypologie());
				 for ($i=0; $i < count($catalogue); $i++)
					include 'objectCatalogue.php';
				?>
		</article>
		<article>
			<button type="submit" id="btnValidCatalogue">Valider la preselection</button>
		<article>
	</main>
	</body>
</html>
