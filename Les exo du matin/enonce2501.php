<!DOCTYPE html>
<html>
<head>
	<title>encode</title>
</head>
<body>
<?php
	function encode($character) {
		$c = ord($character);
		if (!($c % 3)) {
			return chr($c * 2 + 15);
		} else return chr($c);
	}
?>
</body>
</html>
