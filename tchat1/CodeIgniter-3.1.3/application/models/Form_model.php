<?php
class Form_model extends CI_Model {

	public function __construct() {

		$this->load->database();
	}

	// la function de Corentin pour récupérer un pseudo correspondant à un id dans la table utilisateur du tchat Zora

	// public function get_news($edouard) {
	// 	return $this->db->select('id, pseudo')->from('utilisateur')->where('id', $edouard)->get()->row("pseudo");
	// }

	// public function get_news($slug = FALSE)
	// {
	//         if ($slug === FALSE)
	//         {
	//                 $query = $this->db->get('news');
	//                 return $query->result_array();
	//         }

	//         $query = $this->db->get_where('news', array('slug' => $slug));
	//         return $query->row_array();
	// }

	// public function set_news()
	// {
	//     $this->load->helper('url');

	//     $slug = url_title($this->input->post('title'), 'dash', TRUE);

	//     $data = array(
	//         'nom' => $this->input->post('nom'),
	//         'prenom' => $this->input->post('prenom'),
	//         'pseudo' => $this->input->post('pseudo'),
	//         'email' => $this->input->post('email'),
	//     	'password' => $this->input->post('mdp')
	//     );

	//     return $this->db->insert('utilisateur', $data);
	// }

	public function check($login, $mdp) {
		$res = $this->db->query("SELECT pseudo,password FROM utilisateur WHERE pseudo = ? AND password = ?",array($login, $mdp));

		return $res->result();

	}
}