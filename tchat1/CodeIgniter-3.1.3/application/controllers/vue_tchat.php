<?php			

class vue_tchat extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
        $this->load->model('form_model');
        $this->load->helper('url_helper');
        $this->output->enable_profiler(true);
	}
	public function index() {
		$this->load->view('templates/header', $data);
    	$this->load->view('news/tchat');
    	$this->load->view('templates/footer');
	}

}