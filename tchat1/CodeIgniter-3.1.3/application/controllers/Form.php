<?php
class Form extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('form_model');
                $this->load->helper('url_helper');
                $this->output->enable_profiler(true);
        }

        // public function index() {
                
        // $this->load->model('form_model');

        // $data['news'] = $this->form_model->get_news();
        // $data['title'] = 'News archive';

        // $this->load->view('templates/header', $data);
        // $this->load->view('news/index', $data);
        // $this->load->view('templates/footer');
        // }

        // public function view($slug = NULL)
        // {
        //         $data['news_item'] = $this->form_model->get_news();

        //          if (empty($data['news_item'])) {
        //                 show_404();
        //         }

        // $data['title'] = $data['news_item']['title'];

        // $this->load->view('templates/header', $data);
        // $this->load->view('news/views', $data);
        // $this->load->view('templates/footer');
        // }

        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Bienvenue';

            $this->form_validation->set_rules('nom', 'Nom', 'required');
            $this->form_validation->set_rules('prenom', 'Prenom', 'required');
            $this->form_validation->set_rules('pseudo', 'Pseudo', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('mdp', 'Mot de passe', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('news/create');
                $this->load->view('templates/footer');

            }
            else
            {
                $data['title'] = 'Le tchat';

                $this->form_model->set_news();
                
                redirect('vue_tchat/index');
            }
        }

        public function log() {

            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Bienvenue';

            $this->form_validation->set_rules('inscription', 'Login', 'required');
            $this->form_validation->set_rules('mdp', 'Mot de passe', 'required');
            
            $this->form_model->check($_POST['inscription'], $_POST['mdp']);




            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('news/create');
                $this->load->view('templates/footer');

            }
            else
            {
                $data['title'] = 'Enfin on y est';
                redirect('vue_tchat/index');
            }
        }
}