<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('Model_esp');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['numero'] = $this->Model_esp->numeroNomDepartement();
		$this->load->view('vue_esp', $data);
	}

	// public function getListe() {
	// 	$data['numero'] = $this->Model_esp->numeroNomDepartement();
	// 	$this->load->view('vue_esp', $data);
	// }

	public function esperance() {
		
		$departement = $this->input->post('departement');
		$genre = $this->input->post('gender');
		$age = $this->input->post('age');
		// $data['numero'] = $this->Model_esp->numeroNomDepartement();
		if (isset($departement) && isset($genre) && isset($age)) {
			$data['numero'] = $this->Model_esp->numeroNomDepartement();
			$data['esperance'] = $this->Model_esp->calculeEsperance($age, $genre, $departement);
		} else {
			$data['erreur'] = "Veuillez remplir tous les champs.";	
		}
		$this->load->view('vue_esp', $data);
	}
}