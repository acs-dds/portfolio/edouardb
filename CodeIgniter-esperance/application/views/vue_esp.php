
<!DOCTYPE html>
<html>
<head>
	<title>Esperance</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/main.css') ?>">
</head>
<body>
<h3>Bienvenue sur l'app esperance. Cette application s'appuit sur les statistiques de l'INSEE en 2015 et estime votre espérance de vie.</h3>

<form method="post" action="<?php echo base_url() ?>controller/esperance/">
	
		<p><label for="departement">Departement</label>
			<select name="departement">
			  	<?php foreach ($numero as $row){ ?>
			  		<option value="<?php echo $row['noms_departement'] ?>"><?php echo $row['code_departement'].' '.$row['noms_departement'] ?></option>
			  		<?php } ?>
		  	</select><br></p>
		<p><label for="male">Homme</label>
		<input type="radio" name="gender" id="male" value="homme"><br></p>
		<p><label for="female">Femme</label>
		<input type="radio" name="gender" id="female" value="femme"><br></p>
		<p><label for="age">Age</label>
		<input type="number" name="age" id="age" min="0" max="59"><br></p>
		<p><input type="submit" value="Submit"></p>
  	
</form>
<?php if (isset($erreur)) {
	echo $erreur;
}

?>
<?php if (isset($esperance)) {
	echo $esperance;
	}
?>

</body>
<footer>
	
</footer>
</html>