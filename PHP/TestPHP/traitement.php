<?php
   session_start();
   $p = array_map('trim', $_POST);
   $valide = true;
   $erreurs =[];

   if(strlen($p['nom']) < 3) {
      $valide = false;
      $erreurs[] = 'le nom doit contenir un minimum de trois caractères';
   }
   if (strlen($p['prenom']) < 3) {
      $valide = false;
      $erreurs[] = 'le prénom doit contenir un minimum de trois caractères';
   }
   if (preg_match("#^[a-z0-9_.-]+@[a-z0-9-]{2,}\.[a-z]{2,}$#i", $p['email']) === 0) {
      $valide = false;
      $erreurs[] = "format invalide";
   }
   if (strlen($p['message']) < 20 || strpos($p['message'], ' ') === false) {
      $valide = false;
      $erreurs[] = "20 caractères minimum et un espace minimum";
   }

   if (!$valide) {
      $_SESSION['erreurs'] = $erreurs;
      header("Location: formulaire.php");
      exit;
   }

   // if ($_POST == "Edouard") {
   //    # code...
   // }
   
   $dests = [
         'farid' => 'http://faridl.dijon.codeur.online/ProjetFEC/portofolio.php',
         'edouard' => 'http://edouardb.dijon.codeur.online/PHP/portfolio.php',
         'camille' => 'http://camillec.dijon.codeur.online/Portfolio/portfolio.php'
   ];

   $dest = $dests[$_POST['dest']];
   unset($_POST['dest']);
   $get = [];
   foreach ($_POST as $key => $value) {
      $get[] = "$key=".urlencode($value);
   }
   $get = "?".implode('&', $get);	
   	
   header('Location: '.$dest.$get);

   // $result = "Votre message a bien été envoyé!";
?>

