<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Formulaire</title>
</head>
<body>

<h1>Laissez-nous un message</h1>

	<?php if (isset($_SESSION['erreurs'])): ?>
		<ul>
			<?php foreach ($_SESSION['erreurs'] as $erreur): ?>
			<li><?php echo $erreur; ?></li>
			<?php endforeach; unset($_SESSION['erreurs']); ?>
		</ul>
	<?php endif; ?>

    <form method="post" action="traitement.php">
    		<!-- séléction des personnes -->
    	<label for="dest">Destinataire :</label>
    	<select name="dest" id="dest">
	      	<option value ="choix">Choisissez la personne à contacter</option>
	      	<option value ="farid">Farid</option>
	        <option value ="edouard">Edouard</option>
	        <option value ="camille">Camille</option>
      	</select>

	        <label for="nom">Prénom :</label> <input type="text" name="nom" id="nom"/>

	        <label for="prenom">Nom :</label> <input type="text" name="prenom" id="prenom"/>

	        <label for="email">Mail :</label> <input type="text" name="email" id="email">

	        <label for="message">Votre message :</label> <textarea type="text" name="message" id="message"></textarea>

	        <input type="submit" name="envoyer" value="Envoyer"/>
    </form>
</body>
</html>