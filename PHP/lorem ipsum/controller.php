<?php require_once 'mapper.php';


class Controller {

	private $poem;
	
	public function __construct() {
		$this->poem = new Mapper();
	}
	// public function actionGetMot($nb) {
	// return $this->poem->getMot($nb);
	// }
	public function actionGetPara($para, $nb) {
		return $this->poem->getPara($para, $nb);
	}

}
?>