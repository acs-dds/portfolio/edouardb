<?php require_once 'controller.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Controller</title>
	<link rel="stylesheet" type="text/css" href="main.css">
	<link href="https://fonts.googleapis.com/css?family=VT323" rel="stylesheet">
</head>
<body>
	<main>
		<h1 class="un">Lorem Ipsum</h1>
		<h2 class="deux">“Aujourd'hui on peut faire de la musique avec des ordinateurs, mais l'ordinateur a toujours existé dans la tête des compositeurs.”</h2>
		<h2 class="trois">__Milan Kundera__</h2>
		<p class="quatre">Ceci est un générateur de poême aléatoire. Choisissez un nombre de mot par paragraphe et un nombre de paragraphe.</p>
		<form method="post">
			<ul class="cinq">
				<li><input type="number" name="Nmot" placeholder="nombre de mot"></li>
				<li><input type="number" name="Npara" placeholder="nombre de paragraphe"></li>
				<li><div class="space"></div></li>
				<li><input class="valid" type="submit" value="GO"></li>
				<div class="result">
			<?php
				$gets = new Controller();
				if (isset($_POST['Nmot']) && isset($_POST['Npara'])) {
					echo $gets->actionGetPara($_POST['Npara'], $_POST['Nmot']);
				}
			?>
				</div>
			</ul>
		</form>
	</main>
</body>
</html>