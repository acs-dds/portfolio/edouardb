<?php
	class article {
		private $ref;
		private $materiaux;
		private $taille;
		private $epaisseur = [];

		public static function __construct($array) {
			$this->ref = $array[0];
			$this->materiaux = $array[1];
			$this->taille = $array[2];
			$this->epaisseur = $array[3];
		}
		public function getRef() {
			return $this->ref;
		}
	}
?> 