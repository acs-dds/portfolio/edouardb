<!DOCTYPE html>
<html>
<head>
	<title>Formulaire</title>
</head>
<body>
    <form method="post" action="traitement.php">
    	<fieldset>
	    	<select name="envoyeur">
		      	<option value="choix" disabled selected>Choisissez la personne à contacter</option>
		      	<option value="farid">Farid</option>
		        <option value="edouard">Edouard</option>
		        <option value="camille">Camille</option>
	      	</select>
      	</fieldset>
      	<fieldset>
	        <label for="nom">Nom :</label> <input type="text" name="nom" id="nom"/>
	        <label for="prenom">Prenom :</label> <input type="text" name="prenom" id="prenom"/>
	        <label for="message">Votre message :</label> <textarea type="text" name="message" id="message"></textarea>
	    </fieldset>
	    <fieldset>
	        <input type="submit" name="envoyer" value="Envoyer"/>
	    </fieldset>
    </form>

    <?php

	if (isset($_POST['nom'])) {

   		if (strlen($_POST['nom']) < 3) {
	        echo "3 caractères minimum";
	       	exit;
   		}
	}
   
?>

</body>
</html>